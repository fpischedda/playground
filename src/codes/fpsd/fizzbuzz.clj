(ns codes.fpsd.fizzbuzz
  (:require [clojure.core.match :refer [match]]))

(defn fizz-buzz
  "Implements FizzBuzz, returning
- The string Fizz if the number is divisible by 3
- The string Buzz if the number is divisible by 5
- The string FizzBuzz if the number is divisible by both 3 and 5
- The argument if none of the previous conditions are met
"
  [n]
  (match [(mod n 3) (mod n 5)]
         [0 0] "FizzBuzz"
         [0 _] "Fizz"
         [_ 0] "Buzz"
         :else n))

(defn fizz-buzz-cond
  "Implements FizzBuzz, returning
- The string Fizz if the number is divisible by 3
- The string Buzz if the number is divisible by 5
- The string FizzBuzz if the number is divisible by both 3 and 5
- The argument if none of the previous conditions are met
"
  [n]
  (let [m3 (mod n 3)
        m5 (mod n 5)]
    (cond
      (and m3 m5) "FizzBuzz"
      m3 "Fizz"
      m5 "Buzz"
      :else n)))
