(ns codes.fpsd.sse
  (:require [aleph.http :as http]
            [manifold.bus :as b]
            [reitit.ring :as ring]
            [ring.middleware.params :as params]))

;; the one and only event bus needed for this app
;; using defonce to be able to possibly evaluate the
;; full buffer without breaking existing connections
(defonce event-bus (b/event-bus))

(defn format-event
  "Return a properly formatted event payload"
  [body]
  (str "data: " body "\n\n"))

(defn sse-events [_req]
  {:status 200
   :headers {"content-type" "text/event-stream"}
   :body (b/subscribe event-bus "the-chat-room")})

(defn send-message! [request]
  (let [message (format-event (-> request :params (get "message")))]
    (b/publish! event-bus "the-chat-room" message)
    {:status 204
     :headers {:content-type "text/plain"}
     :body ""}))

(defn create-app
  "Return a ring handler that will route /events to the SSE handler
   and that will servr  static content form project's resource/public directory"
  []
  (ring/ring-handler
   (ring/router
    [["/events" {:get {:handler sse-events
                       :name ::events}}]
     ["/send-message" {:post {:name ::send-message
                              :handler send-message!}}]]
     
    {:data {:middleware [params/wrap-params]}})

   (ring/routes
    (ring/create-resource-handler {:path "/"})
    (ring/create-default-handler))
   ))


;; Web server maangement code to make it easy to start and stop a server
;; after changesto router or handlers
(def _server (atom nil))

(defn start-server! []
  (reset! _server (http/start-server (create-app)
                                     {:port 8080
                                      :join? false})))

(defn stop-server! []
  (swap! _server (fn [s]
                   (when s
                     (.close s)))))
(comment
  
  (start-server!)

  (stop-server!)

  (require '[portal.api :as p])

  (def p (p/open))

  (add-tap #'p/submit)
  ,)
