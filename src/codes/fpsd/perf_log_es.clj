(ns codes.fpsd.perf-log-es
  (:require [com.brunobonacci.mulog :as u]
            [taoensso.tufte :as t]
            [integrant.core :as ig]
            [cheshire.core :as json]
            [codes.fpsd.fizzbuzz :as fb]
            [aleph.http :as http]
            [reitit.ring :as ring]
            [ring.middleware.params :as params]))

;; Printing perfs
(comment
  ;; Request to send `profile` stats to `println`:
  (t/add-basic-println-handler! {})

  (t/profile
   {}
   (dotimes [_ 50]
     (t/p :fizz-buzz-match (fb/fizz-buzz 10000))
     
     (t/p :fizz-buzz-cond (fb/fizz-buzz-cond 10000)))))

;; Getting perfs and inspecting pstat
(comment
  (def res (t/profiled
            {}
            (dotimes [_ 50]
              (t/p :fizz-buzz-match (fb/fizz-buzz 10000))
              
              (t/p :fizz-buzz-cond (fb/fizz-buzz-cond 10000)))))

  (nth res 0) ;; => nil
  (deref (nth res 1)) ;; => continues...

  #_{:clock {:t0 30597219635504, :t1 30597220674063, :total 1038559},
     :stats {:fizz-buzz-match {:min 3973, :mean 5973.8, :p75 4196.75, :mad-sum 165725.20000000007, :p99 47871.74999999987, :n 50, :p25 4052.0, :p90 4959.9000000000015, :var 1.1782216812E8, :max 81400, :mad 3314.5040000000013,
                               :loc {:ns "codes.fpsd.perf-log-es", :line 20, :column 14, :file "/home/foca/work/playground/src/codes/fpsd/perf_log_es.clj"},
                               :last 81400, :p50 4116.5, :sum 298690, :p95 6182.649999999998, :var-sum 5.891108406E9},
             :fizz-buzz-cond {:min 3630, :mean 4549.58, :p75 3795.0, :mad-sum 71821.36000000004, :p99 14535.559999999994, :n 50, :p25 3707.0, :p90 4196.1, :var 7170650.4836, :max 15690, :mad 1436.427200000001,
                              :loc {:ns "codes.fpsd.perf-log-es", :line 22, :column 14, :file "/home/foca/work/playground/src/codes/fpsd/perf_log_es.clj"},
                              :last 15690, :p50 3735.5, :sum 227479, :p95 12587.149999999998, :var-sum 3.5853252418E8}}
     }
  )

;; Sending pstats to Portal via tap
(comment
  (let [[res pstats] (t/profiled
                      {}
                      (dotimes [_ 50]
                        (t/p :fizz-buzz-match (fb/fizz-buzz 10000))
                        
                        (t/p :fizz-buzz-cond (fb/fizz-buzz-cond 10000))))]
    (tap> res)
    (tap> @pstats)))

;; accumulate pstats and send them to Portal
(comment
  
  (def accumulated-stats (t/add-accumulating-handler! {}))

  (t/profile
   {:id :testing-1}
   (dotimes [_ 50]
     (t/p :fizz-buzz-match (fb/fizz-buzz 10000))
     (t/p :fizz-buzz-cond (fb/fizz-buzz-cond 10000))))

  (t/profile
   {:id :testing-2}
   (dotimes [_ 50]
     (t/p :fizz-buzz-match (fb/fizz-buzz 20000))
     (t/p :fizz-buzz-cond (fb/fizz-buzz-cond 20000))))

  (doseq [[p-id pstats] @accumulated-stats]
    (tap> [p-id @pstats]))

  ,)

;; web server setup with profiling middleware 
(defn request->endpoint-name
  [request]
  (-> request :reitit.core/match :data (get (:request-method request)) :name))

(defn profiler-middleware
  "Middleware that wraps handlers with a call to t/profile
  using the endpoint name as the id of the "
  [wrapped]
  (fn [request]
    (t/profile {:id :ring-handler}
               (t/p (or (request->endpoint-name request) :unnamed-handler)
                    (wrapped request)))))

(defn fizz-buzz-handler
  "Ring handler that takes the number from the request path-params
  and returns a JSON with the resulf of calling fizz-buzz"
  [request]
  (try
    (let [number (-> request
                     :path-params
                     :number
                     Integer/parseInt)]
      (u/log :fizz-buzz-handler :argument number)
      {:status 200
       :body (json/generate-string {:result (for [n (range 1 (inc number))]
                                              (fb/fizz-buzz n))})
       :content-type "application/json"})
    (catch NumberFormatException e
      (u/log :fizz-buzz-handler :error (str e))
      {:status 400
       :body (json/generate-string {:error (str "Invalid numeric parameter " e)})
       :content-type "application/json"})))

(defn create-app
  "Return a ring handler that will route /events to the SSE handler
   and that will servr  static content form project's resource/public directory"
  []
  (ring/ring-handler
   (ring/router
    [["/fizz-buzz/:number" {:get {:handler fizz-buzz-handler
                                  :name ::fizz-buzz}}]
     ]
     
    {:data {:middleware [profiler-middleware
                         params/wrap-params]}})
   ))

;; define all integrant components

(defmethod ig/init-key :profiler/accumulator [_ config]
  (t/add-accumulating-handler! {}))

(defmethod ig/init-key :mulog/publisher [_ config]
  (u/start-publisher! config))

(defmethod ig/init-key :profiler/drain [_ {:keys [delay accumulator]}]
  (future
    (while true
      (Thread/sleep delay)
      ;; drain pstats if any
      (when-let [stats @accumulator]
        (doseq [[p-id pstats] stats]
          (doseq [[handler handler-stats] (:stats @pstats)]
            (let [{:keys [loc min max mad p95 p99]} handler-stats]
              (u/log :accumulator-drain :perf-id p-id :handler handler)
              (u/log p-id :handler handler :loc loc :min min :max max :mad mad :p95 p95 :p99 p99))))))
    (u/log :accumulator-drain-finished)))
            

(defmethod ig/halt-key! :profiler/drain [_ pstat-future]
  (future-cancel pstat-future))

(defmethod ig/init-key :http/server [_ config]
  (http/start-server (create-app) config))

(defmethod ig/halt-key! :http/server [_ server]
  (.close server))

;; and the configuration for all components of the system
;; being basically all "raw" data it can from an external
;; source like an edn file
(def config {:profiler/accumulator {}
             :profiler/drain {:delay 5000
                              :accumulator (ig/ref :profiler/accumulator)}
             :mulog/publisher {:type :elasticsearch
                               ;; Elasticsearch endpoint (REQUIRED)
                               :url  "https://localhost:9200/"

                               ;; extra http options to pass to the HTTP client
                               :http-opts {:insecure? true
                                           :basic-auth ["admin" "admin"]}}
             
             :http/server {:port 8080
                           :join? false}})

;; prepare the system, it will be intilialized only after
;; de-referencing the system var
(def system
  (delay (ig/init config)))

(comment
  ;; actually init system
  @system

  ;; stop the system
  (ig/halt! @system)

  (u/log ::test-message :something "nice!" :other "value" :num 1))
