(ns codes.fpsd.ring-basic-auth
  "A namespace used to show how the basic-authentication middleware works"
  (:require [clojure.core.cache.wrapped :as cache]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [environ.core :refer [env]]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.basic-authentication :refer [wrap-basic-authentication]]))

;; We want to hold the server instance in order to close it once done.
;; There are better ways to do it, using life cycle/state management liraries
;; like mount, integrant, compenent just to name few, but lets keep it simple
(def server-instance (atom nil))

(defn handler
  "Our basic ring handler function, it takes a request map (unused) and
   return a response map"
  [_request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body "Hello World"})

(defn get-credentials
  "Return a username -> password map reading it form the specified file, or an
   empty map it reading fails"
  [credentials-path]
  (try
    (with-open [r (io/reader credentials-path)]
      (edn/read (java.io.PushbackReader. r)))
    (catch Throwable  _ {})))

;; create a TTL cache with an empty map and a ttl of 60 seconds
(def credentials-cache (cache/ttl-cache-factory {} :ttl 60000))

(defn authenticated?
  [username password]
  (let [credentials (cache/lookup-or-miss
                      credentials-cache
                      :credentials ;; we try to lookup the key :credentials in the cache
                      (fn [_]      ;; if the key is missing or expired we load the credentials file
                        (get-credentials (:basic-auth-credentials env "credentials.edn"))))]
    (= (get credentials username) password)))

(defn gen-app
  []
  (wrap-basic-authentication handler authenticated?))

(defn start-server
  "Starts a web server holding its instance in the server-instance atom"
  []
  (reset! server-instance
          (jetty/run-jetty (gen-app) {:port 3000
                                      :join? false})))

(defn stop-server
  "If there is a server running, stop it and reset the server-instance atom"
  []
  (swap! server-instance
         (fn [inst]
           (when inst
             (.stop inst)))))

(comment
  (start-server)
  (stop-server)
  ,)
